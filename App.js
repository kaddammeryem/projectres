
import React, { useEffect,useMemo,useState } from 'react';
import { StyleSheet, View ,Image, } from 'react-native';

import { SearchBar,Icon,Header,Button,Card,Input} from 'react-native-elements';
import {createStackNavigator} from '@react-navigation/stack';
import Information from './Screens/Informations.js'
import { NavigationContainer } from '@react-navigation/native';
import {createDrawerNavigator,} from '@react-navigation/drawer';
import Restaurant from './Screens/Restaurant.js';
import MesReservations from './Screens/MesReservations.js'
import { Provider, observer, inject } from 'mobx-react'
import Sign from './Screens/Sign.js';
import * as Facebook from 'expo-facebook';
import AsyncStorage from '@react-native-community/async-storage';
import currentUser from './Screens/GetItem'
import Images from './Screens/Images'
import MenuRestau from './Screens/MenuRestau'
import Apropos from './Screens/Apropos' 
import Contacts from './Screens/Contacts'
import MonCompte from './Screens/MonCompte'
import AproposApp from './Screens/AproposApp.js';
import NousContacter from './Screens/NousContacter.js';
import store from './Screens/Store'
import Favoris from './Screens/Favoris'
import CreateAccount from './Screens/CreateAccount.js';
import Test from './Screens/testMenu.js'
import testRestau from './Screens/testRestau';
import testImage from './Screens/testImage.js';
import Acceuil from './Screens/Acceuil'
const Stack=createStackNavigator();
const Drawer=createDrawerNavigator();
const Drawer2=createDrawerNavigator();



class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      isLoading:false,
      userData:null,
      isConnected:false,
      photo:null,
    }
  }
  //pour avoir la valeur de isConnected
  DrawerStyle =({navigation})=>{
    return(
        <View style={{flex:1}}>
          <View style={{flex:1 ,justifyContent:'flex-start',alignItems:'center',backgroundColor:'#DAA520'}}>
            <Image source={require('./assets/LOGOAPP2.png')} style={{marginTop:30}}/>
          </View>
          <View style={{flex:3,justifyContent:'space-evenly',alignItems:'flex-start'}}>
            <Button icon={<Icon name='person' />} title='Mon compte' onPress={()=>navigation.navigate('MonCompte')}titleStyle={{color:'black'}} buttonStyle={{backgroundColor:'white'}} />
            <Button icon={<Icon name='assignment' />} title='Mes Reservations' titleStyle={{color:'black'}} buttonStyle={{backgroundColor:'#white'}} onPress={()=>{navigation.navigate('MesReservations')}}  />
            <Button icon={<Icon name='favorite' />} title='Favoris' titleStyle={{color:'black'}} buttonStyle={{backgroundColor:'white'}} onPress={()=> navigation.navigate('Favoris')} />
            <Button icon={<Icon name='info' />} title='A propos' onPress={()=>navigation.navigate('AproposApp')} titleStyle={{color:'black'}} buttonStyle={{backgroundColor:'white'}} />
            <Button icon={<Icon name='message' />} onPress={()=>navigation.navigate('NousContacter')}  title='Nous contacter' titleStyle={{color:'black'}} buttonStyle={{backgroundColor:'white'}} c/>
          </View>
          <View style={{justifyContent:'flex-start',alignItems:'flex-start',backgroundColor:'#DAA520'}}>
            <Button  icon={<Icon name='lock' size={18}style={{margin:10}}/>} title='Log out' onPress={()=> this.logout()}titleStyle={{color:'black',fontSize:16,fontWeight:'500'}}  buttonStyle={{backgroundColor:'#DAA520'}}/>
          </View>
        </View>
  );
}

  drawerFunction =()=>{
    return (
      <Drawer.Navigator initialRouteName='Acceuil' drawerContent={this.DrawerStyle}>
        <Drawer.Screen name='Acceuil' component={Acceuil} initialParams={{ 'picture':'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=4344657008938209&height=50&width=50&ext=1609359158&hash=AeSy6eL-q9QrHYklBiY' }}/>
        <Drawer.Screen name='MesReservations' component={MesReservations}/>
        <Drawer.Screen name='MonCompte' component={MonCompte} initialParams={{ 'picture':'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=4344657008938209&height=50&width=50&ext=1609359158&hash=AeSy6eL-q9QrHYklBiY'}}/>
        <Drawer.Screen name='AproposApp' component={AproposApp}/>
        <Drawer.Screen name='NousContacter' component={NousContacter}/>
        <Drawer.Screen name='Favoris' component={Favoris}/>
      </Drawer.Navigator>
      );} 
  initAuthToken = async () => {
      //this.setState(authData);
    if (currentUser!=null) {
      this.setState({isConnected:true});
      this.setState({userData:currentUser._55});
    }
    else{
      this.setState({isConnected:false})
      this.setState({userData:null})
    }
  }
  componentDidMount=()=> {  
    this.initAuthToken()
  }
  facebookLogIn = async () => {
      try {
        await Facebook.initializeAsync('897285013968583')
        const {
          type,
          token
        } = await Facebook.logInWithReadPermissionsAsync('897285013968583', {
          permissions: ['public_profile','email'],
        });
        console.log({token})
        if (type === 'success') {         
          const response = await fetch('http://kaddammeryem.pythonanywhere.com/restaures/clientFacebook',{
              method:'POST',
              body:JSON.stringify({token})
          })
          .then((response)=>response.json())
          .then((data)=>{
            console.log(data)
            this.setState({
            userData:{name:data[0].nom_Personne,id:data[0].cle_FB,prenom:data[0].prenom_Personne,email:data[0].mail_Personne,picture:data[0].picture},
            isLoading:true,isConnected:true});//pas cle_FB mais plutot id (a voir comment faire)
            console.log(this.state.userData)
            console.log('user '+this.state.userData.picture)
            this.storeData(this.state.userData);
        })
        } else {
          console.log('cancel')
        }
      } catch ({ message }) {
        alert(` ${message}`);
      }
    }
   storeData = async (data) => {
      try {
        const jsonValue = JSON.stringify(data)
        await AsyncStorage.setItem('userData',  jsonValue)
        await AsyncStorage.setItem('isConnected','true')
       
      // console.log(currentUser)
      } catch (e) {
        console.log('error'+e);
      }
    }

    logout = async () => {
      try {
        await AsyncStorage.setItem('isConnected','false');
        this.setState({isConnected:false})
       }
       catch (e) {
        console.log('error'+e);
      }
    }
   /* signInWithGoogleAsync= async ()=> {
      const result = await Google.logInAsync({
      
      iosClientId: '969809200509-0jdgche9jtdbcuu385b21dnffb4b2o5o.apps.googleusercontent.com',
       scopes: ['profile', 'email'],
     });
     let token=result.accessToken;
     if (result.type === 'success') {
     const response = await fetch('http://kaddammeryem.pythonanywhere.com/restaures/clientGoogle',{
      method:'POST',
      body:JSON.stringify({token})
     })
    }
    }
*/
 /*try {
        const result = await Google.logInAsync({
           androidClientId: '969809200509-rh7c6865pqcgtfr8nthj9lihv8g9cile.apps.googleusercontent.com',
           iosClientId: '969809200509-cpag12r14tm6sof5d6gh7nu6jcmk4odk.apps.googleusercontent.com',
          scopes: ['profile', 'email'],
        });
    
        if (result.type === 'success') {
   
          const token=result.accessToken;
          console.log(token)
          console.log(result.user)
          const response = await fetch('http://kaddammeryem.pythonanywhere.com/restaures/clientGoogle',{
            method:'POST',
            body:JSON.stringify({token})
        })
        .then(this.setState({isConnected:true}))
        dans le back end on fait http au serveur du service google authUrl:https://accounts.google.com/o/oauth2/v2/auth?
    client_id=969809200509-on6ie5pqc1mllk3gso5ujaib87vvpj2j.apps.googleusercontent.com
 
    &redirect_uri=http://kaddammeryem.pythonanywhere.com/restaures/clientGoogle
&scope=https://www.googleapis.com/auth/cloud-platform.read-only
    &response_type=token
        .then((response)=>response.json())
        .then((data)=>{
            this.setState({
              userData:{name:data.name,id:data.id,prenom:data.prenom,email:data.email},
             isLoading:true,isConnected:true});
       console.log(result.user.id)
        })    
      } 

      else{
      }
      }  catch (e) {
        return { error: true };
      }*/
    
  render(){
    if(this.state.isConnected===true){  
      return (
        <Provider  store={store}>
          <NavigationContainer>
            <Stack.Navigator  initialRouteName='Acceuil'
                              screenOptions={{headerShown: false}} >
              <Stack.Screen name='Sign' component={Sign}/>
              <Stack.Screen name='Acceuil' component={this.drawerFunction} />
              <Stack.Screen name='Information' component={Information} />
              <Stack.Screen name='Restaurant' component={Restaurant} />
              <Stack.Screen name='MesReservations' component={MesReservations}/>
              <Stack.Screen name='Images' component={Images}/>
              <Stack.Screen name='MenuRestau' component={MenuRestau}/>
              <Stack.Screen name='Apropos' component={Apropos}/>
              <Stack.Screen name='Contacts' component={Contacts}/>
              <Stack.Screen name='CreateAccount' component={CreateAccount}/>
              <Stack.Screen name='testRestau' component={testRestau}/>
              <Stack.Screen name='testImage' component={testImage}/>
            </Stack.Navigator>
          </NavigationContainer>
        </Provider>
      )}
 
  else{
    return( 
      <Sign onpress={()=>this.facebookLogIn()}  />
     ) }
   }}


export default App;
      
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});



