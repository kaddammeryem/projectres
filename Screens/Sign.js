import React from 'react';
import {Text, View ,Image} from 'react-native';
import {Icon,Header,Button, }from 'react-native-elements';
 import {SocialIcon} from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler';





export default class Sign extends React.Component{
        constructor(props){
                super(props);
                this.state={
                       onpress:props.onpress,
                       onpressGoogle:props.onpressGoogle
                 }                       
        }
        
                             
        render(){    
                return(
                        <View style={{flex :1,flexDirection:'column'}}>
                                <View style={{
                                        flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                        <Header
                                                placement="right"
                                                leftComponent={ <Button onPress={()=>{navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                                centerComponent={<Text style={{fontWeight:'500',marginRight:'30%',fontSize:20}}>Welcome</Text>}
                                                flex={1}
                                                backgroundColor='#DAA520'
                                                rightComponent={<Button iconRight={true} icon={<Icon name='skip-next' type='material-community' marginTop={3}/>} title='skip'titleStyle={{color:'black'}} buttonStyle={{backgroundColor:'#DAA520'}}/>}
                                        />

                                </View>
                        
                                <View  style={{flex:4,flexDirection:'column',alignItems:'center',justifyContent:'flex-start',marginTop:40}}>
                                        <Image style={{}}source={require('../assets/LOGOAPP2.png')}/>
                                        <View style={{flex:3,alignItems:'center'}}>
                                                <SocialIcon
                                                        title='Sign In With Facebook'
                                                        button
                                                        type='facebook'
                                                        onPress={this.state.onpress}
                                                        style={{width:'90%',margin:'20%',backgroundColor:'#DAA520',padding:'20%'}}
                                                />
                                        </View>
                                </View>             
                        </View>
                );}}

