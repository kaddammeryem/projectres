import React  from 'react';
import{View,Text,StyleSheet,Image,ScrollView} from 'react-native';
import AuthContext from '../App';
import {Header,Button, Divider} from 'react-native-elements'


export default class Images extends React.Component{
    constructor(props){
        super(props);
      
        console.log(this.props.route.params.id)
    }
    
    render(){
       
        return(
            <View style={{flex :1,flexDirection:'column'}}>
                       <View style={{
                               flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                        centerComponent={<Text style={{fontWeight:'500',marginRight:'50%',fontSize:20}}>{this.props.route.params.restaur}</Text>}
                                        flex={1}
                                        backgroundColor='#DAA520'
                                />

                        </View>
                        <ScrollView contentContainerStyle={{width:'100%',justifyContent:'center',alignItems:'center'}}>
                          
                        

                      
                        <View style={styles.container}>
                            <View>
                                    <Image style={{ resizeMode: "cover",height: 150, width: 300,margin:15}} source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/1'}}/>
                            </View>
                            <View>
                                    <Image style={{ resizeMode: "cover",height: 150, width: 300,margin:15}} source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/2'}}/>
                            </View>
                            <View>
                                    <Image style={{ resizeMode: "cover",height: 150, width: 300,margin:15}} source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/3'}}/>
                            </View>
                            <View>
                                    <Image style={{ resizeMode: "cover",height: 150, width: 300,margin:15}} source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/4'}}/>
                            </View>
                            <View>
                                    <Image style={{ resizeMode: "cover",height: 150, width: 300,margin:15}} source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/5'}}/>
                            </View>
                        </View>
         
                      
                            
                       
                           
                       
                        </ScrollView>

                     

            </View>);
    }
}
const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    height: "100%",
    textAlign: "center"
  }
});

/*
   <View style={{flex:1}}>
                            <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/2'}}/>
                        </View>
                        <View style={{flex:1}}>
                            <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/3'}}/>
                        </View>
                        <View style={{flex:1}}>
                            <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/4'}}/>
                        </View>
                        <View style={{flex:1}}>
                            <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.props.route.params.id)+'/5'}}/>
                        </View>

*/


/*

                            <Image source={{uri:this.state.url+'/2'}} />
                    
                        
                            <Image source={{uri:this.state.url+'/4'}} />
                         
                            <Image source={{uri:this.state.url+'/5'}} />
                           
                            <Image source={{uri:this.state.url+'/3'}} />
*/