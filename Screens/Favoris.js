import React ,{Component}from 'react';
import { StyleSheet, Text, View ,Image,TouchableOpacity,ScrollView,FlatList } from 'react-native';
//Reservez.js 
import { StatusBar } from 'expo-status-bar';
import { SearchBar,Icon,Header,Button,Card,Input, Divider,Rating, ThemeConsumer} from 'react-native-elements';
import {createStackNavigator} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Restaurant from './Restaurant.js'
import App from '../App.js';

import { DrawerItem } from '@react-navigation/drawer';
import {decorate,observer, inject,Provider} from 'mobx-react';
import {observable, action,computed} from "mobx"
import store from './Store'
import currentUser from './GetItem';
import Menu from './Menu'
import Acceuil from './Acceuil.js';



export default class Favoris extends React.Component{
     /*renderElementsEmpty=()=>{
         return(
             <View style={{flex:1,alignItems:'center'}}>
                 <Text style={{fontSize:18,color:'grey'}}>No favorite restaurants selected</Text>
             </View>
         );
     }*/

    /* renderElements=({item})=>{
        if(item==undefined){
            return null
        }
         else{  
            console.log('Hello');         
        return(
            <View style={{flex:1,justifyContent:'space-evenly',alignItems:'center',margin:6}}> 
                <Menu  idRestau={item.id} nameRestau={item.name} adresseRestau={item.adresse} 
                categorieRestau={item.categorie}  />
            </View>     
         );
        }
      
        }*/
    
      render(){
          //console.log(this.props.store.favorites)
          return(
              <View style={{flex :1,flexDirection:'column'}}>
                    <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                        <Header
                                placement="right"
                                leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                centerComponent={<Text style={{fontWeight:'500',marginRight:'50%',fontSize:20}}>Favoris</Text>}
                                flex={1}
                                backgroundColor='#DAA520'
                        />

                    </View>

                  { /* <FlatList
                   
                    data={this.props.store.favorites}
                    
                    renderItem={this.renderElements}
                    extraData={this.props.store.favorites.length}
                    numColumns={1}/>*/
                   
              
                  }
                   <View style={{flex:2,alignItems:'center'}}>
                        <Card containerStyle={{padding:0,width:'95%',height:'49%',flexDirection:'column',justifyContent:'space-between',borderRadius:'3%'}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('testRestau')}style={{width:'100%',height:'75%',padding:5}} >
        
       
                            <Image 
                                source={require('../assets/restaurantImage.jpg')} 
                                style={{width :'100%',
                                height:'100%',
                                borderRadius:3}} />
                            
                        
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                <Text style={{fontSize:23,margin:8}}>Marly</Text>
                                <View style={{flexDirection:'row'}}>
                                        <Rating     
                                                                    
                                                style={{marginLeft:8,marginTop:20}}
                                                type='custom'
                                                imageSize={10}
                                                ratingCount={4}/>
                                                                                            
                                        <Text   style={{fontSize:13,
                                                color:'grey',
                                                marginLeft:8,
                                                marginTop:17}}>4.5</Text>

                                        <Text   style={{fontSize:13,
                                                color:'grey',
                                                marginLeft:8,
                                                margin:8,
                                                marginTop:17}}>(453)</Text>
                            </View>
                            </View>
                            <Text   style={{fontSize:14,
                                    color:'black',
                                    margin:8}}>$ · Italian,Cafe</Text>
                            <Text   style={{fontSize:14,
                                    color:'black',
                                    marginLeft:8}}>P · Kesh</Text>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                    <Text   style={{fontSize:14,
                                            color:'grey',
                                            margin:8,
                                            marginTop:14}}>Small plates,salades and sandwishs... </Text>
                                    <Button  
                                            buttonStyle={{backgroundColor:null}}  
                                            icon={<Icon name='favorite' 
                                                        color='#DAA520' 
                                                        size='20'/>}/>     
                        
                            </View> 
                        
                  
                    
                    
                    </TouchableOpacity>
                </Card>
            </View>
         
          </View>
          )


      }
          
       
       
        
}
      
        
       
           
          
         
     
       
                
    