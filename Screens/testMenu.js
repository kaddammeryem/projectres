import React from 'react';
import {View,Text,Image,TouchableOpacity,StyleSheet,} from 'react-native';
import {Icon,Button,Card,Rating} from 'react-native-elements';


export default class MenuTest extends React.Component{
    constructor(props) {
        super(props);
        this.state = { 
            nameRestau:this.props.nameRestau,
            adresseRestau:this.props.adresseRestau,
            categorieRestau:this.props.categorieRestau,
            onpress:props.onpress,
            idRestau:props.idRestau,
            rating:props.rating,
            nbrRating:props.nbrRating,
            villeRestau:props.villeRestau ,
            descriptionRestau:props.descriptionRestau, 
        }
       
    }
    render(){
        var description=this.state.descriptionRestau;
        var shortDescription=description.slice(0,35);
        
      
        return( 
            <View style={{flex:1 , flexDirection:'column'}}>
                <Card containerStyle={{padding:0,width:'95%',height:300,flexDirection:'column',justifyContent:'space-between',borderRadius:'3%'}}>
                    <TouchableOpacity  onPress={this.state.onpress} style={{width:'100%',height:'75%',padding:5}} >
                        <Image 
                           source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restaus/image/'+String(this.state.idRestau)+'/5'}}
                           style={{width :'100%',
                           height:'100%',
                           borderRadius:3}}      
                        />
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={{fontSize:23,margin:8}}>{this.props.nameRestau}</Text>
                            <View style={{flexDirection:'row'}}>
                                <Rating
                                     style={{marginLeft:8,marginTop:20}}
                                     type='custom'
                                     imageSize={10}
                                     ratingCount={4} 
                                     /*onFinishRating={(result)=>{
                                         fetch('http://kaddammeryem.pythonanywhere.com/restaures/rating',{method:'POST',
                                             body:JSON.stringify({'idRestau':this.state.idRestau,'rating':result}) })
                                             .then(response=>response.json())
                                             .then(json=>{this.setState({rating:json.rating,nbrRating:json.nbrRating});console.log(json)})
                                         }} */
                                />
                                <Text  style={{fontSize:13, color:'grey',marginLeft:8,marginTop:17}}>
                                    {this.state.rating}
                                </Text>
                                <Text   style={{fontSize:13,color:'grey', marginLeft:8,margin:8,marginTop:17}}>
                                    ({this.state.nbrRating})
                                </Text>                                                                                        
                            </View>
                        </View>
                        <Text   style={{fontSize:14,color:'black',margin:8}}>
                            $ · {this.state.categorieRestau}
                        </Text>
                        <Text   style={{fontSize:14,color:'black',marginLeft:8}}>
                            P · {this.state.adresseRestau}
                        </Text>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <Text   style={{fontSize:14,color:'grey',margin:8,marginTop:14}}>
                            
                                {shortDescription}...
                            </Text>
                            <Button buttonStyle={{backgroundColor:null}} 
                                    icon={<Icon name='favorite' color='#DAA520' size={20}/>}
                            />                        
                        </View> 
                    </TouchableOpacity>        
                </Card>
            </View>
    )}
}

const styles = StyleSheet.create({
    title:{
        fontSize: 14,
        fontWeight:'bold',
        color: "white",
    },
    row:{
        flexDirection: 'row',
        justifyContent:'space-between',
        height:56,
        paddingLeft:25,
        paddingRight:18,
        alignItems:'center',
        backgroundColor: "white",
    },
    parentHr:{
        height:1,
        color:"white",
        width:'100%'
    },
    child:{
        backgroundColor: "white",
        padding:16,
       
    }
});