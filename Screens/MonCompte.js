import React, { useContext, useEffect } from 'react';
import { StyleSheet, Text, View ,Image} from 'react-native';
import { SearchBar,Icon,Header,Button,Card,Input,Avatar,Divider} from 'react-native-elements';
import { render } from 'react-dom';
import currentUser from './GetItem'





 export default class MonCompte extends React.Component{
   constructor(props){
     super(props);
     this.state={
             picture:this.props.route.params.picture
     }
     
    
   }
   
   render(){
    let currentUser2 = JSON.parse(currentUser._55)
    let firstLetterName=currentUser2.name[0]
    let firstLetterPrenom=currentUser2.prenom[0]
     return(
       <View style={{flex:1}}>
             <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                        centerComponent={<Text style={{fontWeight:'500',marginRight:'50%',fontSize:20}}>Mon compte</Text>}
                                        flex={1}
                                        backgroundColor='#DAA520'
                                />

               </View>
               <View style={{flex:2, alignItems:'center',justifyContent:'flex-start'}}>
    
                       <Card    containerStyle={{width:'95%',height:'70%',justifyContent:'center',alignItems:'flex-start',shadowColor:'#DAA520',
                                shadowRadius: 2 ,flexDirection:'row'}} dividerStyle={{backgroundColor:'#DAA520'}}> 
                                <View style={{flexDirection:'row',padding:20}}>
                                        <Avatar rounded size='medium' 
                                                 source={{uri:'https://scontent.frba1-1.fna.fbcdn.net/v/t1.0-9/89787150_3585678251502759_827809608629223424_o.jpg?_nc_cat=111&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGTEs78cXv1dbkYC8gYB1QJ3C_TMiqcnobcL9MyKpyehrYJYL8nhF9vOxzlPbqIo0q3iXkMZlOc6THxcIg76dBk&_nc_ohc=LfeIGBGg4BQAX_u9Oaj&_nc_ht=scontent.frba1-1.fna&oh=690deb3f58acad097f532cf6a32d21b4&oe=60492708'}}
                                                title= {firstLetterName+firstLetterPrenom}backgroundColor='#DAA520'/>
                                        <Text style={{fontSize:18,fontWeight:'bold',margin:15}}>Informations Personnelles</Text>
                                </View>

       
                      <View style={{flexDirection:'column'}}>
                                <View style={{flexDirection:'row',margin:20}}>
                                        <Text style={{fontSize:15,fontWeight:'300'}}>Nom   </Text>     
                                        <Text style={{fontSize:15,fontWeight:'500',marginLeft:20}}>{currentUser2.name}</Text>
                                                      
                                
                                </View>
                                <Divider style={{backgroundColor:'#DAA520'}}/>
                                <View style={{flexDirection:'row',margin:20}}>
                                        <Text style={{fontSize:15,fontWeight:'300'}}>Prenom   </Text>
                                        <Text style={{fontSize:15,fontWeight:'500'}}>{currentUser2.prenom}</Text>

                                </View>
                                <Divider style={{backgroundColor:'#DAA520'}}/>
                                <View style={{flexDirection:'row',margin:20}}>
                                                <Text style={{fontSize:15,fontWeight:'300'}}>Email </Text>
                                                 <Text style={{fontSize:15,fontWeight:'500',marginLeft:26}}>{currentUser2.email} </Text>
                                </View>
                                <Divider style={{backgroundColor:'#DAA520'}}/>
                                <View style={{flexDirection:'row',margin:20}}>
                                        <Text style={{fontSize:15,fontWeight:'300'}}>Pays   </Text>
                                        <Text style={{fontSize:15,fontWeight:'500',marginLeft:26}}>Maroc  </Text>
                                </View>
                                <Divider style={{backgroundColor:'#DAA520'}}/>
                                <View style={{flexDirection:'row',margin:20}}>
                                        <Text style={{fontSize:15,fontWeight:'300'}}>Ville </Text>
                                        <Text style={{fontSize:15,fontWeight:'500',marginLeft:35}}>Casablanca  </Text>
                                </View>
                           </View>
                        </Card>
                </View>
      </View>
     )
   }
 }