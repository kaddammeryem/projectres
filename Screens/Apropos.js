import React  from 'react';
import{View,Text,StyleSheet,Image,ScrollView} from 'react-native';
import AuthContext from '../App';
import {Header,Button, Divider,Card} from 'react-native-elements'


export default class Apropos extends React.Component{
  
    render(){
       
        return(
            <View style={{flex :1,flexDirection:'column'}}>
                       <View style={{
                               flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                        centerComponent={<Text style={{fontWeight:'500',marginRight:'40%',fontSize:20}}>A propos</Text>}
                                        flex={1}
                                        backgroundColor='#DAA520'
                                />

                        </View>

                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Card   containerStyle={{width:'70%',height:'40%',justifyContent:'center',alignItems:'center',shadowColor:'#DAA520',shadowRadius: 2 }}
                     title=  {this.props.route.params.restaur} titleStyle={{color:'#DAA520',fontWeight:'900',fontSize:25 }} dividerStyle={{backgroundColor:'#DAA520'}}> 
                     <Text style={{fontSize:22}}>
                         {this.props.route.params.apropos}
                     </Text>
                     </Card>

                        </View>
             </View>)}}