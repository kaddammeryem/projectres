import React from 'react';
import { StyleSheet, Text, View ,Image,TextInput,TouchableOpacity,ScrollView } from 'react-native';
//Restaurant.js 
import { StatusBar } from 'expo-status-bar';
import { SearchBar,Icon,Header,Button,Card,SocialIcon,Avatar} from 'react-native-elements';
import Information from './Informations.js';
import App from '../App.js';
import MapView  from 'react-native-maps';
import * as Linking from 'expo-linking';
import { Link } from '@react-navigation/native';






export default class testRestau extends React.Component{
        
        constructor(props){
                super(props);
                
              
              

        }
        render(){

        return(
            <View style={{flex:1,flexDirection:'column'}}>
                       <View style={{
                               flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                        centerComponent={<Text style={{fontWeight:'500',marginRight:'40%',fontSize:20}}>Restaurants</Text>}
                                        flex={1}
                                        backgroundColor='#DAA520'
                                />

                        </View>
                        <ScrollView>
                                <Card >  
                                        <Text> Un texte donnant des details sur les services et les plats</Text>
                                </Card>
                                <View style={{alignItems:'center'}}>


                               
                                        <Text style={{fontSize:23}}>Images</Text>
                                
                      
                                       <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                           <Image source={require('../assets/8_1.bmp')} style={{width:'50%'}}/>

                                           
                                           <Image source={require('../assets/8_2.bmp')}/>
                                       </View>
                                       <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                           <Image source={require('../assets/8_3.bmp')} style={{width:'30%'}}/>

                                           
                                           <Image source={require('../assets/8_4.bmp')} style={{width:'30%'}}/>
                                           <Image source={require('../assets/8_5.bmp')} style={{width:'30%'}}/>
                                       </View>
                              </View>
            
                        

                                <View style={{alignItems:'center'}}>
                
                                        <Text style={{fontSize:23}}> Menu</Text>

               
                                </View>          
                              
                      
            <View style={{flex:2}}>         
            <MapView style={{width:'100%' ,margin:10}} onPress={()=>Linking.openURL('https://www.google.com/maps/search/?api=1&query=33.33,-7')} />
            </View>    

            <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:23}}>Contacts</Text>
            </View>
  
               <View style={{flex:1,flexDirection:'row'}}>
                       <SocialIcon type='facebook'/>
                      
                        <SocialIcon type='instagram'/>
                     
              
                        <SocialIcon type='twitter'/>
                   
            
                        <Avatar rounded size="medium" icon={{ name: 'phone' }} activeOpacity={0.7} containerStyle={{backgroundColor:'#DAA520',marginLeft:12}}/>
                      
               </View>
        


     


                        
                      
           
                        
                </ScrollView>
        </View>);}}

const styles = StyleSheet.create({
    container: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
     borderColor:'#DAA520',

     borderWidth:2,

    
      
      textAlign: "center"
    },
    container2: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
        alignItems: "center",

     
    margin:10,
        textAlign: "center"
      }
  });
  