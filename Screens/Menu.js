import React ,{Component}from 'react';
import { StyleSheet, Text, View ,Image,TouchableOpacity,ScrollView,Picker} from 'react-native';
import { SearchBar,Icon,Header,Button,Card,Input, Divider,Rating, ThemeConsumer} from 'react-native-elements';


export default class Menu extends React.Component{ 
    constructor(props){
        super(props);
        this.state={
            nameRestau:props.nameRestau,
            adresseRestau:props.adresseRestau,
            categorieRestau:props.categorieRestau,
            onpress:props.onpress,
            idRestau:props.idRestau,
            rating:props.rating,
            nbrRating:props.nbrRating,
            villeRestau:props.villeRestau           
        }
        console.log('ici c est menu non test')      
 }
render()
    
    { 
      
return(
       
                <Card containerStyle={{width:'95%',height:'100%',padding:5,borderRadius:'5%'}}>
                        <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-between',width:'100%',height:150}} onPress={this.state.onpress}>
                                <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restauimage/'+String(this.state.idRestau)+'/5'}} style={{width :'45%',height:164}} resizeMode='cover'/>
                                <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>                
                                                <View style={{flexDirection:'column',justifyContent:'space-between',alignItems:'flex-start'}}>
                                                        <Text style={{fontsize:16,fontWeight:'500',marginTop:8}} >{this.state.nameRestau}</Text>
                                                        <View style={{flexDirection:'row'}}>
                                                                <Rating
                                                                marginLeft='5'
                                                                type='custom'
                                                                imageSize={13}
                                                                ratingCount={4}
                                                                onFinishRating={result=>{fetch('http://kaddammeryem.pythonanywhere.com/restaures/rating',
                                                                {method:'POST',
                                                                body:JSON.stringify({'idRestau':this.state.idRestau,'rating':result}) })
                                                                .then(response=>response.json())
                                                                .then(json=>{this.setState({rating:json.rating,nbrRating:json.nbrRating});console.log(json)})
                                                                }}/>
                                                                <Text style={{fontSize:14,fontWeight:'400',marginLeft:6}}>0.1</Text>
                                                        </View>
                                                        <View style={{flexDirection:'row'}}>
                                                                        <Icon  name='location-city' size='18'>  </Icon>
                                                                        <Text style={{fontsize:14,marginLeft:10}} >{this.state.villeRestau}</Text>
                                                         </View>
                                                         <View style={{flexDirection:'row'}}>
                                                                        <Icon  name='location-on' style={{marginRight:10}} size='18'>  </Icon>
                                                                        <Text style={{fontsize:14}} >{this.state.adresseRestau}</Text>
                                                         </View>
                                                         <View style={{flexDirection:'row'}}>
                                                                        <Icon style={{marginRight:10}}name='bookmark' size='18'>  </Icon>
                                                                        <Text style={{fontsize:14}}>{this.state.categorieRestau}</Text>
                                                         </View>
                                                                
                                                </View>
                                                
                                </View>
                                 <View style={{flexDirection:'column',alignSelf:'flex-start'}}>
                                       
                                       <Button  icon={<Icon  name='favorite' color='#DAA520' size={20}/>}
                                       buttonStyle={{backgroundColor:null}}
                                       onPress={()=>{let restau={ 'id':this.state.idRestau,'name':this.state.nameRestau,'categorie':this.state.categorieRestau,'adresse':this.state.adresseRestau}
                                                       if(this.props.store.searchElement(restau)===0){
                                                              
                                                               this.props.store.pushElement(restau);
                                                        }
                                                       else{
                                                            
                                                               this.props.store.deleteElement(restau);
                                                             
                                                       }  
                                               }}          
                                       />                 
                               </View>
                         </TouchableOpacity>

                </Card>

);

      
        }
}