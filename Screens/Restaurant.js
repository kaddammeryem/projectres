
import React from 'react';
import { StyleSheet, Text, View ,Image,TextInput,TouchableOpacity } from 'react-native';
//Restaurant.js 
import { StatusBar } from 'expo-status-bar';
import { SearchBar,Icon,Header,Button,Card} from 'react-native-elements';
import Information from './Informations.js';
import App from '../App.js';






export default class Restaurant extends React.Component{
        
        constructor(props){
                super(props);
        }
        render(){

        return(
            <View style={{flex :1,flexDirection:'column'}}>
                       <View style={{
                               flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                        centerComponent={<Text style={{fontWeight:'500',marginRight:'40%',fontSize:20}}>Restaurants</Text>}
                                        flex={1}
                                        backgroundColor='#DAA520'
                                />

                        </View>

                    
                           
                        <View style={{flex:2 ,alignItems:'center'}}>
                                <Image source={require('../assets/ReservedImage.jpg')} resizeMethod ="scale" style={{width:'100%',height:'80%'}}/>
                         </View>
                         <View style={{flex:2,flexDirection :'row',justifyContent:'space-around'}}>
                                
                                <Card containerStyle={{width:'25%',height:'58%', padding : 0}} > 
                                        
                                                       
                                                        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('MenuRestau',{'restaur':this.props.route.params.restau,'id':this.props.route.params.id}) }}>
                                                        <View style={{backgroundColor:'#DAA520' ,width:'100%',height:'80%',alignItems:'center',justifyContent:'center'}}>
                                                                <Image source={require('../assets/MenuLogo2.png')} style={{width:'100%',height:'100%',resizeMode:'contain'}}/>
                                                        </View>
                                                        <View style={{justifyContent:'center',alignItems:'center',padding: 2}}>
                                                                <Text style={{fontWeight: '500',fontSize:16}}>Menu</Text>
                                                        </View>
                                                </TouchableOpacity>
                                        
                                </Card>
                                
                                <Card containerStyle={{width:'25%',height:'58%', padding : 0}} > 
                                                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Information',{'restaur':this.props.route.params.restau,'id':this.props.route.params.id,'adresse':this.props.route.params.adresse}) }}>
                                                        <View style={{backgroundColor:'#DAA520' ,width:'100%',height:'80%',alignItems:'center',justifyContent:'center'}}>
                                                                <Image source={require('../assets/ChairIcon.png')}  style={{width:'100%',height:'100%',resizeMode:'contain'}}/>
                                                        </View>
                                                        <View style={{justifyContent:'center',alignItems:'center',padding: 2}}>
                                                                <Text style={{fontWeight: '500',fontSize:16}}>Reservez</Text>
                                                        </View>
                                                </TouchableOpacity>
                                </Card>
                                
                        
                        
                        </View> 
            

                                <View style={{flex:2,flexDirection :'row',justifyContent:'space-around'}}>
                                


                                        <Card containerStyle={{width:'25%',height:'58%', padding : 0}} > 
                                                
                                                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Images',{'restaur':this.props.route.params.restau,'id':this.props.route.params.id}) }}>
                                                                <View style={{backgroundColor:'#DAA520' ,width:'100%',height:'80%',alignItems:'center',justifyContent:'center'}}>
                                                                        <Image source={require('../assets/ImageIcon.png')}  resizeMethod="scale"style={{width:'100%',height:'100%',resizeMode:'contain'}}/>
                                                                </View>
                                                                <View style={{justifyContent:'center',alignItems:'center',padding: 2}}>
                                                                        <Text style={{fontWeight: '500',fontSize:16}}>Images</Text>
                                                                </View>
                                                        </TouchableOpacity>
                                                
                                        </Card>
                                                
                                        <Card containerStyle={{width:'25%',height:'58%', padding : 0}} > 
                                                
                                                              
                                                                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Contacts',{'latitude':this.props.route.params.latitude,'longitude':this.props.route.params.longitude,'restaur':this.props.route.params.restau,'apropos':this.props.route.params.apropos,'facebook':this.props.route.params.facebook,'instagram':this.props.route.params.instagram,'twitter':this.props.route.params.twitter,'telephone':this.props.route.params.telephone,'adresse':this.props.route.params.adresse,'ville':this.props.route.params.ville})}}>
                                                        <View style={{backgroundColor:'#DAA520' ,width:'100%',height:'80%',alignItems:'center',justifyContent:'center'}}>
                                                                <Image source={require('../assets/PhoneIcon.png')}  style={{width:'80%',height:'100%',resizeMode:'contain'}}/>
                                                        </View>
                                                        <View style={{justifyContent:'center',alignItems:'center',padding: 2}}>
                                                                <Text style={{fontWeight: '500',fontSize:16}}>Contacts</Text>
                                                        </View>
                                                </TouchableOpacity>
                                                
                                        </Card>
                                     
                                      
                                
                                
                                </View> 
                       
            </View>
        );
                       }
}