import React  from 'react';
import{View,Text,StyleSheet,Image,ScrollView} from 'react-native';
import AuthContext from '../App';
import {Header,Button, Divider,Card,Icon,SocialIcon,Avatar} from 'react-native-elements'


export default class Apropos extends React.Component{
  
    render(){
       
        return(
            <View style={{flex :1,flexDirection:'column'}}>
                       <View style={{
                               flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                        centerComponent={<Text style={{fontWeight:'500',marginRight:'30%',fontSize:20}}>Nous contacter</Text>}
                                        flex={1}
                                        backgroundColor='#DAA520'
                                />

                        </View>

                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Card   containerStyle={{width:'90%',height:'60%',justifyContent:'space-between',alignItems:'center',shadowColor:'#DAA520',shadowRadius: 2 }}
                                     dividerStyle={{backgroundColor:'#DAA520'}}> 
                            <View style={{flex:2,flexDirection:'row'}}>
                                    <SocialIcon type='facebook'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                      facebook
                                    </Text >
                            </View>
                            <View style={{flex:2,flexDirection:'row'}}>
                            <SocialIcon type='instagram'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                         instagram
                                    </Text>
                            </View>
                            <View style={{flex:2,flexDirection:'row'}}>
                            <SocialIcon type='twitter'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                      Twitter
                                    </Text>
                             </View>
                             < View style={{flex:2,flexDirection:'row'}}>
                                 <Avatar rounded size="medium" icon={{ name: 'phone' }} activeOpacity={0.7} containerStyle={{backgroundColor:'#DAA520',marginLeft:12}}/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16,marginLeft:8}}>
                                      06789
                                    </Text>
                            </View>
                     
                     </Card>

                        </View>
             </View>)}}