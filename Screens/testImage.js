import React from 'react';
import { StyleSheet, Text, View ,Image,TextInput,TouchableOpacity,ScrollView ,SafeAreaView,ImageBackground} from 'react-native';
import { Icon,Header,Button,Card,SocialIcon,Avatar,} from 'react-native-elements';
import MapView  from 'react-native-maps';
import * as Linking from 'expo-linking';







export default class testImage extends React.Component{
    constructor(props){
        super(props);  
        this.state={
            nbr:this.props.route.params.nbr,
        } 
    }
    onPressSuiv=()=>{
        var nombre=parseInt(this.state.nbr)+1;
        console.log('before'+this.state.nbr)
        if(this.state.nbr!=5){
            this.setState({nbr:nombre})
            console.log('after'+this.state.nbr)
        }
      
        
    }
    onPressPreced=()=>{
        var nombre=parseInt(this.state.nbr)-1;
        console.log('before'+this.state.nbr)
        if(this.state.nbr!=1){
            this.setState({nbr:nombre})
            console.log('after'+this.state.nbr)
        }
      
        
    }
    
    render(){
        return(
            <View style={{flex:1,flexDirection:'column'}}>
                <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                    <Header
                         placement="right"
                         leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                         centerComponent={<Text style={{fontWeight:'500',marginRight:'40%',fontSize:20}}>Restaurants</Text>}
                         flex={1}
                         backgroundColor='#DAA520'
                    />
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',flex:1,backgroundColor:'black'}}> 
                    <Icon name='arrow-left-drop-circle-outline' type='material-community' color='white' onPress={()=>this.onPressPreced()}/>     
                    <Image source={{uri:'https://kaddammeryem.pythonanywhere.com/restaures/restaus/image/'+String(this.props.route.params.id)+'/'+this.state.nbr}} style={{width:'85%',height:'90%',justifyContent: "center"}} />
                     <Icon name='arrow-right-drop-circle-outline' type='material-community' color='white' onPress={()=>this.onPressSuiv()}/>
                </View>
            </View>);}}

const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        borderColor:'#DAA520',
        borderWidth:2,
        textAlign: "center"
    },
    overlay: {
        backgroundColor:'rgba(255,0,0,0.5)',},
    container2: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
        alignItems: "center",
        margin:10,
        textAlign: "center"
      },
    
  });
  