import React from 'react';
import {Text,View,Image}from 'react-native';
import {Input,Header,Button,Icon} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';


export default class CreateAccount extends React.Component{

        constructor(props){
            super(props);
            this.state={
                nom:'',
                prenom:'',
                identifiant:'',
                mdp:''
            }
        }


        storeData = async (data) => {
            try {
              const jsonValue = JSON.stringify([[this.state.identifiant,this.state.nom,this.state.prenom,this.state.mdp]])
              await AsyncStorage.setItem('userData',  jsonValue)
              
             
            // console.log(currentUser)
            } catch (e) {
              console.log('error'+e);
            }
          }
render(){
    return(
       
    <View style={{flex :1,flexDirection:'column',justifyContent:'flex-start',alignItems:'stretch'}}>
            <View style={{flexDirection:'row' }} >

                  <Header 
                        leftComponent={<Button onPress={()=>{this.props.navigation.goBack()}}  solid buttonStyle={{backgroundColor:'#DAA520'} }
                          icon={<Icon name="arrow-back"></Icon>}></Button> }
                          flex={1}
                          backgroundColor='#DAA520'
                        centerComponent={<Text style={{fontWeight:'500',fontSize:20}}>Create Account</Text>}

                                            
                   />
            </View>
            <View style={{}}>
                     <View style={{justifyContent:'center',alignItems:'center',margin:10}}>
                    <Text style={{fontWeight:'700',fontSize:23}}>Informations</Text>
                    </View>
                    <View style={{justifyContent:'center',alignItems:'center'}}>
                    <Input  placeholder='Nom'
                            leftIcon={ <Icon
                            name='person'
                            size={24}
                            color='black'
                                        />}
                            onChangeText={(change)=>this.setState({nom:change})}/>
                    <Input placeholder='Prénom'
                            leftIcon={ <Icon
                            name='person'
                            size={24}
                            color='black'
                                        />}
                            onChangeText={(change)=>this.setState({prenom:change})}/>
                    <Input placeholder='Identifiant'
                            leftIcon={ <Icon
                            name='account-box'
                            size={24}
                            color='black'
                                        />}
                             onChangeText={(change)=>this.setState({identifiant:change})}/>
                    <Input placeholder='mot de passe'
                            leftIcon={ <Icon
                            name='lock'
                            size={24}
                            color='black'
                                        />}
                            onChangeText={(change)=>this.setState({mdp:change})}/>
                    <View style={{justifyContent:'center',alignItems:'center'}}>
                    <Button title='Create'   buttonStyle={{backgroundColor:'#DAA520',width:'100%'} } onPress={()=>{
                        fetch('http://kaddammeryem.pythonanywhere.com/restaures/client/',
                        {method:'POST',
                    body:JSON.stringify({
                        'name':this.state.nom,
                        'prenom':this.state.prenom,
                        'identifiantClient':this.state.identifiant,
                    })})

                        
                    }} />
                    </View>
                    </View>

            
            </View>
    </View>
    )
}


}
