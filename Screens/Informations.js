import React from 'react';
//APP.js car premiere page
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View ,TextInput ,Image,ScrollView} from 'react-native';
import { SearchBar,Icon,Header,Button,Input,Divider} from 'react-native-elements';
import  '../assets/restaurantImage.jpg';
import App from '../App.js';
import DrawerStyle from '../DrawerStyle.js';
import DatePicker from 'react-native-datepicker';
import Acceuil from './Acceuil';
import { inject ,Provider} from 'mobx-react';
import AsyncStorage from '@react-native-community/async-storage';
import currentUser from './GetItem';







export default class Information extends React.Component{
 
  constructor(props){
      super(props);
      this.state={
        
        tele:'',
        nbr:1,
        date:'',
        time:'',
        infos:null,
        
      }
    }
           
 render(){
  let user = JSON.parse(currentUser._55)
  console.log(user.id);
  return(
    
      <View style={{flex :1,flexDirection:'column',justifyContent:'flex-start',alignItems:'stretch'}}>
              <View style={{flexDirection:'row' }} >

                  <Header 
                        leftComponent={<Button onPress={()=>{this.props.navigation.goBack()}}  solid buttonStyle={{backgroundColor:'#DAA520'} }
                          icon={<Icon name="arrow-back"></Icon>}></Button> }
                          flex={1}
                          backgroundColor='#DAA520'
                         centerComponent={<Text style={{fontWeight:'500',fontSize:20}}>Reservez</Text>}

                                            
                   />
                            
                  
              </View>
              
             
         
              <View style={{flex:2,flexDirection:'column',alignItems:'center',justifyContent:'flex-start'}}>
                        <Image source={require('../assets/LOGOAPP2.png')}/>
               
                    <View style={{flexDirection:'row'}}>
                          <Icon name='phone' style={{marginTop:10,marginLeft:25}}/>
                          <Input inputStyle={{ fontSize:16}} placeholder='Numero de telephone'  onChangeText={(change)=>this.setState({tele:change})}></Input>
                    </View>
                    
                  <View style={{flexDirection:'row'}}>
                          <Icon name='group' style={{marginTop:10,marginLeft:25}}/>
                          <Input inputStyle={{ fontSize:16}} placeholder='Nombre de personnes' onChangeText={(change)=>this.setState({nbr:change})} ></Input>
                  </View>
                  
                  <View style={{flexDirection:'column',justifyContent:'space-between'}}>
                      <View style={{flexDirection:'row'}}>
                          <Icon name='date-range' style={{marginTop:10,marginLeft:25}}/>
                          <DatePicker 
                                style={{width: '98.6%',marginBottom:20}}
                                date={this.state.date}
                                
                                mode="date"
                                placeholder="Select date"
                                format="YYYY-MM-DD"
                                minDate="2020-05-01"
                                maxDate="2020-05-09"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                
                                customStyles={{
                                dateInput: {
                                marginLeft: 5,
                                borderWidth: 0,
                                borderBottomWidth:1,
                                borderColor:'grey',
                                margin:0,
                                fontSize:16,
                                fontWeight:'500'
                                
                                },
                                placeholderText: {
                                  marginRight:297,
                                  fontSize:16,
                                  fontWeight:'500'
                                },
                               
                                }}
                                onDateChange={(change) => {this.setState({date:change})}}
                        />
                        
                      </View>
            
     
                      <View style={{flexDirection:'row'}}>
                          <Icon name='schedule' style={{marginTop:18,marginLeft:25}}/>
                          <DatePicker
                                 style={{width: '98.6%',marginBottom:10,marginleft:30}}
                                date={this.state.time}
                                mode="time"
                                placeholder="Select time"
                                format="hh:mm:ss"
                                minDate="08h"
                                maxDate="20h"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                               
                                customStyles={{
                                  
                                
                                dateInput:{
                                marginTop: 15,
                                marginLeft: 5,
                                borderWidth: 0,
                                borderBottomWidth:1,
                                borderColor:'grey',
                               
                                borderBottomColor:'grey',
                                
                                fontSize:16,
                                fontWeight:'500'

                                
                                },
                                
                                placeholderText: {
                                  marginRight:297,
                                  fontSize:16,
                                  fontWeight:'500'
                                }
                                
                                }}
                                onDateChange={(change) => {this.setState({time:change})}}
                          />
                      </View>
                  </View>
                  <View style={{justifyContent:'space-between',alignItems:'center'}}>
                           
                            <Button onPress={()=>{fetch('http://kaddammeryem.pythonanywhere.com/restaures/reservation',{
                            method:'POST',
                            body:JSON.stringify(
                              {
                              'id':user.id,
                              'telePersonne':this.state.tele,
                              'nbr':this.state.nbr,
                              'date':this.state.date,
                              'time':this.state.time,
                              'nameRestau':this.props.route.params.restaur}
                              )
                          }); console.log(this.state.infos);alert('Votre réservation a été réussie');this.props.navigation.navigate('MesReservations')}}
                       
                            title='Reserver' titleStyle={{fontWeight:'500',fontSize:14}} 
                            buttonStyle={{backgroundColor:'#DAA520',marginTop:30,width:200}}></Button>
                  </View> 
             
              </View>
     
             
                
      </View>
  
      
  );
  

}} 

/*
       
<View style={{justifyContent:'space-between',alignItems:'center'}}>
                           
<Button onPress={()=>{fetch('http://meryemkaddam.pythonanywhere.com/restaures/clients',{
                            method:'POST',
                            body:JSON.stringify({
                              
                              'date':this.state.date,
                              'time':this.state.time,
                            })});this.props.navigation.navigate('Reservation') ;alert('Votre réservation a été réussie');this.props.navigation.goBack()}}title='Reserver' titleStyle={{fontWeight:'bold'}} 
                            buttonStyle={{backgroundColor:'#DAA520',marginBottom:10,width:200}}></Button>
</View> 

  */
  


/*
<Button onPress={() =>{ fetch('http://meryemkaddam.pythonanywhere.com/restaures/clients',{
  method:'POST',
  body:JSON.stringify({
    
    'nomPersonne':this.state.nom,
    'prenomPersonne':this.state.prenom,
    'telePersonne':this.state.tele,
    'mailPersonne':this.state.mail})});
    this.props.navigation.navigate('Reservation')} }
    title="Solid Button" title='Continuer' buttonStyle={{backgroundColor:'#DAA520'} } containerStyle={{width:'60%'}}></Button>
*/


/*
<Icon name='schedule' style={{marginTop:8,marginLeft:40}}></Icon>
<DatePicker 
      style={{width: '100%',marginBottom:20}}
      date={this.state.date}
      
      mode="date"
      placeholder="Select date"
      format="YYYY-MM-DD"
      minDate="2020-05-01"
      maxDate="2020-05-09"
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      showIcon={false}
      customStyles={{
      dateInput: {
      borderBottomWidth:1,
      borderWidth: 0,
      borderBottomColor:'grey',
      margin:0
      
      },
      placeholderText: {
        marginRight:328,
        fontSize:20
      }
      }}
      onDateChange={(change) => {this.setState({date:change})}}
/>*/




/*
 <Input placeholder='Nom' leftIcon={{name:'person'}} onChangeText={(change)=>{this.setState({nom:change})}}></Input>
                  <Input placeholder='Prenom' leftIcon={{name:'person'}}  onChangeText={(change)=>this.setState({prenom:change})}></Input>

                  <Input placeholder='Numero de telephone' leftIcon={{name:'phone'}} onChangeText={(change)=>this.setState({tele:change})}></Input>
                  <Input placeholder='Email' leftIcon={{name:'email'}}  onChangeText={(change)=>this.setState({mail:change})}></Input>
                  <Input placeholder='Nombre de personnes' leftIcon={{name:'group'}}  onChangeText={(change)=>this.setState({nbr:change})} ></Input>
                  
                  <View style={{flexDirection:'column',justifyContent:'space-between'}}>
                      <View style={{flexDirection:'row'}}>
                          <DatePicker 
                                style={{width: '98.6%',marginBottom:20}}
                                date={this.state.date}
                                
                                mode="date"
                                placeholder="Select date"
                                format="YYYY-MM-DD"
                                minDate="2020-05-01"
                                maxDate="2020-05-09"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                
                                customStyles={{
                                dateInput: {
                                marginLeft: 5,
                                borderWidth: 1,
                                borderColor:'grey',
                                margin:0
                                
                                },
                                placeholderText: {
                                  
                                  fontSize:20
                                },
                               
                                }}
                                onDateChange={(change) => {this.setState({date:change})}}
                        />
                        
                      </View>
            
     
                      <View style={{flexDirection:'row'}}>
                     
                          <DatePicker
                                 style={{width: '98.6%',marginBottom:20,marginleft:30}}
                                date={this.state.time}
                                mode="time"
                                placeholder="Select time"
                                format="hh:mm:ss"
                                minDate="08h"
                                maxDate="20h"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                               
                                customStyles={{
                                  
                                
                                dateInput:{
                                marginTop: 30,
                                marginLeft: 5,
                                borderWidth: 1,
                                borderColor:'grey',
                               
                                borderBottomColor:'grey',
                                
                                fontSize:30

                                
                                },
                                
                                placeholderText: {
                                  marginRight:0,
                                  fontSize:20
                                }
                                
                                }}
                                onDateChange={(change) => {this.setState({time:change})}}
                          />
                      </View>
                  </View>
                  <View style={{justifyContent:'space-between',alignItems:'center'}}>
                           
                            <Button onPress={()=>{fetch('http://meryemkaddam.pythonanywhere.com/restaures/clients',{
                            method:'POST',
                            body:JSON.stringify({
                              'nomPersonne':this.state.nom,
                              'prenomPersonne':this.state.prenom,
                              'telePersonne':this.state.tele,
                              'mailPersonne':this.state.mail,
                              'nbr':this.state.nbr,
                              'date':this.state.date,
                              'time':this.state.time,
                            })});this.props.navigation.goBack() ;alert('Votre réservation a été réussie');this.props.navigation.goBack()}}title='Reserver' titleStyle={{fontWeight:'bold'}} 
                            buttonStyle={{backgroundColor:'#DAA520',marginTop:30,width:200}}></Button>
                  </View> 
*/