import React from 'react';
import {  View ,FlatList} from 'react-native';
import { SearchBar,Icon,Header,Button,Avatar} from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';
import { ScrollView } from 'react-native-gesture-handler';
import MenuTest from './testMenu.js';
import Menu from './Menu.js';



export default class Acceuil extends React.Component {
       
   constructor(props){
      super(props);
      this.state={
         picture:this.props.route.params.picture,
         donnees:[],
         search:'',
         onpress2:props.onpress2,
         ville:[
            { value:'All'},
            { value:'Casablanca'},
            {value:'Fes'},
            {value:'Marrakech'},
            {value:'Salé'},
            {value:'Rabat'},
            {value:'Kénitra'},
            {value:'Tanger'},
            {value:'Tanger'},
         ],
         categorie:[  
            {value:'All'},
            {value:'Moderne'},
            {value:'Thailandais'},
            {value:'Marocain'},
            {value:'Nigerien'},
            {value:'Chinois'},
            {value:'Chinois'},
            {value:'Café'},
         ],       
         citySelected:'All',
         categorySelected:'All',          
            }
            console.log(this.props.route.params.picture);
       
   }
   fetching=()=>{
      fetch('https://kaddammeryem.pythonanywhere.com/restaures/restaus', {
         method: 'GET'
      })
      .then((response) => response.json())
      .then((json)=>{this.setState({donnees:json});console.log('json'+this.state.donnees)}
      )
     // .then(()=>this.props.store.initiate())
      .catch((error) => {
         console.error(error);
      })
      
   }
   componentDidMount = () => {    
      this.fetching();
     }
   filterFunction=()=>{
      fetch('http://kaddammeryem.pythonanywhere.com/restaures/filter', {
         method: 'POST',
         body:JSON.stringify({'city':this.state.citySelected,'category':this.state.categorySelected})
      })
      .then((response) => response.json())
      .then((json)=>{this.setState({donnees:json})})
      .catch((error) => {
         alert('No matching results')
      })}
     /* categoryFunction=()=>{
         fetch('http://kaddammeryem.pythonanywhere.com/restaures/category', {
            method: 'POST',
            body:JSON.stringify({'category':this.state.categorySelected})
         })
         .then((response) => response.json())
         .then((json)=>{this.setState({donnees:json})})
         .catch((error) => {
            alert('No matching results')
         })}*/
   searchFunction=()=>{
      fetch('http://kaddammeryem.pythonanywhere.com/restaures/search', {
         method: 'POST',
         body:JSON.stringify({'search':this.state.search})
      })
      .then((response) => response.json())
      .then((json)=>{this.setState({donnees:json});})
      .catch((error) => {
         alert('No matching results')
      })}
      
   renderElements=({item})=>{    
      return(
         <View style={{flex:1}}>
            <MenuTest nbrRating={item.nbrRating}rating={item.rating} villeRestau={item.ville_Restau} idRestau={item.id} nameRestau={item.nom_Restau} adresseRestau={item.adresse_Restau} categorieRestau={item.categorie_Restau} descriptionRestau={item.a_propos} 
                  onpress={()=> {this.props.navigation.navigate('testRestau',{'latitude':item.latitude,'longitude':item.longitude,'ville':item.ville_Restau,'restau':item.nom_Restau,'adresse':item.adresse_Restau,'id':item.id,'apropos':item.a_propos,'facebook':item.facebook,'instagram':item.instagram,'twitter':item.twitter,'telephone':item.telephone})}}/>
         </View> );
   }
   render ( )
    {   
     /* let currentUser2 = JSON.parse(currentUser._55)
      let firstLetterName=currentUser2.name[0]
      let firstLetterPrenom=currentUser2.prenom[0]*/
   return(
      <View style={{flex:1 , flexDirection:'column'}}>
         <View style={{flex:0,flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
            <Header
               placement="right"
               leftComponent={ <Button icon={<Icon name='menu'/>} buttonStyle= {{backgroundColor:'#DAA520'}}  onPress={()=> this.props.navigation.toggleDrawer()}/>}
               centerComponent={<SearchBar
               inputStyle={{backgroundColor:'#DAA520',fontSize:16,color:'black'}}
               inputContainerStyle={{backgroundColor:'#DAA520',width:'80%',height:'0%',marginBottom:17}}
               containerStyle={{backgroundColor:'#DAA520',borderBottomColor:'#DAA520',borderTopColor:'#DAA520',marginBottom:14}}             
               placeholder='Type name'              
               placeholderchangeColor='white'
               value={this.state.search}
               onEndEditing={()=>this.searchFunction}            
               onChangeText={(change) => {
                 if(change!=''){
                       this.setState({search:change})
                 }
                 else{
                        this.componentDidMount()
                   }
                  }}
               onClear={()=>this.componentDidMount()}
              />}
               rightComponent={<Avatar rounded size="small" 
               source={{ uri:'https://scontent.frba1-1.fna.fbcdn.net/v/t1.0-9/89787150_3585678251502759_827809608629223424_o.jpg?_nc_cat=111&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGTEs78cXv1dbkYC8gYB1QJ3C_TMiqcnobcL9MyKpyehrYJYL8nhF9vOxzlPbqIo0q3iXkMZlOc6THxcIg76dBk&_nc_ohc=LfeIGBGg4BQAX_u9Oaj&_nc_ht=scontent.frba1-1.fna&oh=690deb3f58acad097f532cf6a32d21b4&oe=60492708'}} 
               titleStyle={{color:'black'}} activeOpacity={0.7} containerStyle={{backgroundColor:'white'}}/>}
               flex={1}
               backgroundColor='#DAA520'                          
            />
         </View>
         {/*<View style={{flexDirection:'row',justifyContent:'space-around',backgroundcolor:'green'}}>
                  <Dropdown  data={this.state.ville}
                  containerStyle={{width:'40%',height:'40%'}}
                  itemCount='3'
                  label='Ville'
                  onChangeText={(change)=> {this.setState({citySelected:change}) ;this.filterFunction()}}/>
                  <Dropdown  data={this.state.categorie}
                  containerStyle={{width:'40%',height:'40%'}}
                  itemCount='3'
                  label='Categorie'
                  onChangeText={(change)=> {this.setState({categorySelected:change}) ;this.filterFunction()}}/>
               </View>*/}
            <View style={{flex:1}}>
               <FlatList
                     data={this.state.donnees}
                     renderItem={this.renderElements}
                     numColumns={1}
               />
            </View>        
    </View>
    ); 

}}