import React from 'react';
import MapView  from 'react-native-maps';

import { StyleSheet, Text, View,TouchableOpacity } from 'react-native';
import{Button,Header,Icon,Card,SocialIcon,Avatar} from 'react-native-elements';
import * as Linking from 'expo-linking';
import { Link } from '@react-navigation/native';

//https://www.google.com/maps/search/?api=1&query=Marly+Casablanca
export default class MapTest extends React.Component {
    constructor(props){
        super(props);
        this.state={
            latitude:this.props.route.params.latitude,
            longitude:this.props.route.params.longitude,
          
            
        }
        
    }
    
  render() {
      console.log(this.state.restau+ " "+this.state.adresse+" "+this.state.ville)
    return (
        <View style={{ flex: 1,backgroundColor: '#fff',alignItems: 'center',justifyContent: 'center'}}>
             <View style={{flexDirection:'row' }} >

                    <Header 
                        leftComponent={<Button onPress={()=>{this.props.navigation.goBack()}}  solid buttonStyle={{backgroundColor:'#DAA520'} }
                            icon={<Icon name="arrow-back"></Icon>}></Button> }
                            flex={1}
                            backgroundColor='#DAA520'
                            centerComponent={<Text style={{fontWeight:'500',fontSize:20}}>Contacts</Text>}

                                    
                        />
                    

            </View>
                   <MapView style={{width:'100%' , height:'25%'}} onPress={()=>Linking.openURL('https://www.google.com/maps/search/?api=1&query='+this.state.latitude+','+this.state.longitude)} />
      

                   <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                       <Button title="click" onPress={()=> this.props.navigation.navigate('test')}/>

                      
                            <Card   containerStyle={{width:'90%',height:'90%',justifyContent:'space-between',alignItems:'center',shadowColor:'#DAA520',shadowRadius: 2 }}
                                     title=  {this.props.route.params.restaur} titleStyle={{color:'#DAA520',fontWeight:'900',fontSize:25 }} dividerStyle={{backgroundColor:'#DAA520'}}> 
                            <View style={{flex:2,flexDirection:'row'}}>
                                    <SocialIcon type='facebook'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                        {this.props.route.params.facebook}
                                    </Text >
                            </View>
                            <View style={{flex:2,flexDirection:'row'}}>
                            <SocialIcon type='instagram'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                    {this.props.route.params.instagram}
                                    </Text>
                            </View>
                            <View style={{flex:2,flexDirection:'row'}}>
                            <SocialIcon type='twitter'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                        {this.props.route.params.twitter}
                                    </Text>
                             </View>
                             < View style={{flex:2,flexDirection:'row'}}>
                                 <Avatar rounded size="medium" icon={{ name: 'phone' }} activeOpacity={0.7} containerStyle={{backgroundColor:'#DAA520',marginLeft:12}}/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16,marginLeft:8}}>
                                        {this.props.route.params.telephone}
                                    </Text>
                            </View>
                     
                     </Card>

                        </View>


          
    
          
        
     
      </View>
    );
  }
}

