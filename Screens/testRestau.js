import React from 'react';
import { StyleSheet, Text, View ,Image,TouchableOpacity,ScrollView ,SafeAreaView,ImageBackground} from 'react-native';
import { Icon,Header,Button,Card,SocialIcon,Avatar,} from 'react-native-elements';
import MapView  from 'react-native-maps';
import * as Linking from 'expo-linking';
import { FlatList } from 'react-native-gesture-handler';



export default class testRestau extends React.Component{
        constructor(props){
                super(props);
                this.state={menu:[]}
        }
        fetching=()=>{
                fetch('https://kaddammeryem.pythonanywhere.com/restaures/menus/restau/'+this.props.route.params.id, {
                   method: 'GET'
                })
                .then((response) => response.json())
                .then((json)=>{this.setState({menu:json});console.log(json)}
                )
               // .then(()=>this.props.store.initiate())
                .catch((error) => {
                   console.error(error);
                })
                
             }
             componentDidMount(){
                     this.fetching();
             }
        renderElement=({item})=>{
              return(
                <View style={{flexDirection:'row',justifyContent:'space-between',margin:10}}>                               
                        <Text style={{fontSize:14,fontWeight:'500'}}>{item.produit.libele_Produit}</Text>  
                        <Text style={{fontSize:14,fontWeight:'500',}}>{item.produit.prix_Produit}</Text>               
                </View>)
        }
        render(){
                return(      
                <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                        centerComponent={<Text style={{fontWeight:'500',marginRight:'40%',fontSize:20}}>Restaurants</Text>}
                                        flex={1}
                                        backgroundColor='#DAA520'
                                />
                        </View>
                        <MapView style={{width:'100%' ,height:'20%'}} onPress={()=>Linking.openURL('https://www.google.com/maps/search/?api=1&query='+this.props.route.params.latitude+','+this.props.route.params.longitude)} />
                        <SafeAreaView style={{flex:1}}>
                                <ScrollView >
                                        <View style={{alignItems:'center'}}>
                                                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',margin:5}}>
                                                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('testImage',{'id':this.props.route.params.id,'nbr':'5'})} style={{margin:10}}>
                                                                <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restaus/image/'+String(this.props.route.params.id)+'/5'}} style={{width:200,height:180,resizeMode:'stretch'}} />
                                                        </TouchableOpacity>
                                                        <TouchableOpacity style={{margin:5}} onPress={()=>this.props.navigation.navigate('testImage',{'id':this.props.route.params.id,'nbr':'4'})}>
                                                                <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restaus/image/'+String(this.props.route.params.id)+'/4'}} style={{width:200,height:180,resizeMode:'stretch'}} />
                                                        </TouchableOpacity>                                                
                                                </View>
                                                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                                                        <TouchableOpacity style={{margin:5}} onPress={()=>this.props.navigation.navigate('testImage',{'id':this.props.route.params.id,'nbr':'3'})}>
                                                                <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restaus/image/'+String(this.props.route.params.id)+'/3'}} style={{width:200,height:180,resizeMode:'stretch'}} />
                                                        </TouchableOpacity>                                                      
                                                        <TouchableOpacity style={{margin:5}} onPress={()=>this.props.navigation.navigate('testImage',{'id':this.props.route.params.id,'nbr':'2'})}>
                                                                <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restaus/image/'+String(this.props.route.params.id)+'/2'}} style={{width:180,height:180,resizeMode:'stretch'}} />
                                                        </TouchableOpacity>
                                                        <TouchableOpacity style={{margin:5}} onPress={()=>this.props.navigation.navigate('testImage',{'id':this.props.route.params.id,'nbr':'1'})}>
                                                                <Image source={{uri:'http://kaddammeryem.pythonanywhere.com/restaures/restaus/image/'+String(this.props.route.params.id)+'/1'}} style={{width:200,height:180,resizeMode:'stretch'}} />
                                                        </TouchableOpacity>                                                                  
                                                </View>
                                        </View>
                                        <View style={{margin:10}}>
                                                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}> 
                                                        <ImageBackground source={require('../assets/menuBack.jpg')} style={{width:'100%',height:600,justifyContent:'space-between'}}>
                                                                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                                                                         <Text style={{fontSize:23,fontWeight:'500'}}>Menu</Text>
                                                                </View>
                                                                <FlatList
                                                                data={this.state.menu}
                                                                renderItem={this.renderElement}
                                                                extraData={this.state.menu.length}
                                                                />                                                                                         
                                                        </ImageBackground>
                                                        <View style={{alignItems:'center'}}>
                                                                <Text style={{fontSize:23}}>Contacts</Text>
                                                                <View style={{flexDirection:'row'}}>
                                                                        <SocialIcon type='facebook' onPress={()=>Linking.openURL(this.props.route.params.facebook)}/>
                                                                        <SocialIcon type='instagram' onPress={()=>Linking.openURL(this.props.route.params.instagram)}/>
                                                                        <SocialIcon type='twitter' onPress={()=>Linking.openURL(this.props.route.params.twitter)}/>
                                                                        <Avatar rounded size="medium" icon={{ name: 'phone' }} activeOpacity={0.7} containerStyle={{backgroundColor:'#DAA520',marginLeft:12,marginTop:9}}/>
                                                                </View>
                                                        </View>
                                                </View>
                                        </View>
                                </ScrollView>
                        </SafeAreaView>
                        <View style={{alignItems:'center'}}>
                                <Button title='Réserver' buttonStyle={{backgroundColor:'#DAA520',padding:10}} containerStyle={{width:130}} onPress={()=>this.props.navigation.navigate('Information')}/>
                        </View>
                </View>);}}

const styles = StyleSheet.create({
    container: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
     borderColor:'#DAA520',

     borderWidth:2,

    
      
      textAlign: "center"
    },
    overlay: {
        backgroundColor:'rgba(255,0,0,0.5)',},
    container2: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
        alignItems: "center",

     
    margin:10,
        textAlign: "center"
      },
    
  });
  