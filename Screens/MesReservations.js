import React from 'react';
//APP.js car premiere page
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View ,TextInput ,Image,FlatList} from 'react-native';
import { SearchBar,Icon,Header,Button,Input,Divider,Card} from 'react-native-elements';
import  '../assets/restaurantImage.jpg';
import App from '../App.js';
import DrawerStyle from '../DrawerStyle.js'






export default class MesReservations extends React.Component{
    constructor(props){
        super(props);
        this.state={
            donnees:[]
        }
       
    }
    componentDidMount=()=>{
        fetch('https://kaddammeryem.pythonanywhere.com/restaures/reservations',{
            method:'GET',
        })
        .then((response)=>response.json())
        .then((json)=>{
            
            console.log(json)
            this.setState({donnees:json})
           
        })
        
    }
    
    renderElements=({item})=>{
        return(
        <View style={{flex:2, alignItems:'center',justifyContent:'flex-start'}}>
    
            <Card   containerStyle={{width:'100%',height:'95%',justifyContent:'space-between',alignItems:'flex-start',shadowColor:'#DAA520',shadowRadius: 2 }}
                     title= {item.nameRestau} titleStyle={{color:'#DAA520',fontWeight:'500',fontSize:16 }} dividerStyle={{backgroundColor:'#DAA520'}}> 

               
               
                <Text style={{fontSize:14,padding:20}}>
                    Date de la réservation :  {item.date}
                    
                </Text>
                <Divider style={{backgroundColor:'#DAA520'}}/>
                <Text style={{fontSize:14,padding:20}}> 
                    Heure de la réservation :   {item.time}
                </Text>
                <Divider style={{backgroundColor:'#DAA520'}}/>
            
             <Text style={{fontSize:14,padding:20}}> 
                    Adresse du restaurant :   {item.adresse} 
                </Text>
                <Divider style={{backgroundColor:'#DAA520'}}/>

                <Text style={{fontSize:14,padding:20}}>
                    Nombre de Personnes :   {item.nbr}
                    
                </Text>
                
             </Card>
         </View>
         ) }
 render(){
  return(
      <View style={{flex:1,flexDirection:'column',justifyContent:'flex-start',alignItems:'center'}}>
            <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={ <Button onPress={()=>{this.props.navigation.goBack()}}icon ={{name :'arrow-back'}} buttonStyle={{backgroundColor:'#DAA520'}} ></Button> }
                                       centerComponent={<Text style={{fontWeight:'500',marginRight:'30%',fontSize:20}}>Mes réservations</Text>}
                                       flex={1}
                                       backgroundColor='#DAA520'
                                />

            </View>
            <FlatList
            data={this.state.donnees} 
            renderItem={this.renderElements}
            extraData={this.state.donnees.length}
            nombre={1}

            />
         
           
                   
      </View>
  )}}