import React from 'react';
import MapView  from 'react-native-maps';

import { StyleSheet, Text, View,TouchableOpacity } from 'react-native';
import{Button,Header,Icon,Card,SocialIcon,Avatar} from 'react-native-elements';
import * as Linking from 'expo-linking';
import { Link } from '@react-navigation/native';

//https://www.google.com/maps/search/?api=1&query=Marly+Casablanca
export default class MapTest extends React.Component {
    constructor(props){
        super(props);
        this.state={
            region:{latitude:33.588609,longitude:-7.644967}
        }
    }
    
  render() {
    return (
        <View style={{ flex: 1,backgroundColor: '#fff',alignItems: 'center',justifyContent: 'center'}}>
             <View style={{flexDirection:'row' }} >

                    <Header 
                        leftComponent={<Button onPress={()=>{this.props.navigation.goBack()}}  solid buttonStyle={{backgroundColor:'#DAA520'} }
                            icon={<Icon name="arrow-back"></Icon>}></Button> }
                            flex={1}
                            backgroundColor='#DAA520'
                            centerComponent={<Text style={{fontWeight:'500',fontSize:20}}>Position</Text>}

                                    
                        />
                    

            </View>
                   <MapView style={{width:'100%' , height:'30%'}} onPress={()=>Linking.openURL('https://www.google.com/maps/search/?api=1&query=Marly+Casablanca')} />
      

                   <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Card   containerStyle={{width:'100%',height:'80%',justifyContent:'space-between',alignItems:'center',shadowColor:'#DAA520',shadowRadius: 2 }}
                                     title=  'Marly' titleStyle={{color:'#DAA520',fontWeight:'900',fontSize:25 }} dividerStyle={{backgroundColor:'#DAA520'}}> 
                            <View style={{flex:2,flexDirection:'row'}}>
                                    <SocialIcon type='facebook'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                      facefdjshfkjdsnfld
                                    </Text >
                            </View>
                            <View style={{flex:2,flexDirection:'row'}}>
                            <SocialIcon type='instagram'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                    instajfdkhgruehsjkfsdgvfj
                                    </Text>
                            </View>
                            <View style={{flex:2,flexDirection:'row'}}>
                            <SocialIcon type='twitter'/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16}}>
                                       twitter.comkjdhsfjkdnf
                                    </Text>
                             </View>
                             < View style={{flex:2,flexDirection:'row'}}>
                                 <Avatar rounded size="medium" icon={{ name: 'phone' }} activeOpacity={0.7} containerStyle={{backgroundColor:'#DAA520',marginLeft:12,marginTop:8}}/>
                                    <Text style={{fontSize:18,fontWeight:'400',marginTop:16,marginLeft:8}}>
                                        0667891478
                                    </Text>
                            </View>
                     
                     </Card>

                        </View>
        
          
    
          
        
     
      </View>
    );
  }
}

